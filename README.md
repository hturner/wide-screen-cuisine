# Wide Screen Cuisine

## Requirements

- nodejs
- npm
- ng-common
- angular cli

These can be installed by running

```bash
sudo apt install nodejs
sudo apt install npm
sudo apt install ng-common
npm install -g @angular/client
```

## Instructions

There are four seperate servers that need to be run: each individual web service and the main web client.
Each service should be ran in a separate terminal.

1. Cuisine App (Port 3100)
    ```bash
    cd cuisineApp/cuisineServer
    npm i
    node server.js
    ```
1. Pairing App (Port 3000)
    ```bash
    cd pairingApp/pairingServer
    npm i
    node server.js
    ```
1. Recipe App (Port 3200)
    ```bash
    cd recipeApp/recipeServer
    npm i
    node server.js
    ```
1. Web Client (Port 4200)
    ```bash
    cd angular-client
    npm i
    ng serve -o
    ```

Once each service has been initialised and is running on separate terminals, you can access the client through the URL <http://localhost:4200/>