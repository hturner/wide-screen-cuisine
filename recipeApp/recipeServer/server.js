// make GET request to recipe API using meal, this returns a recipe ID
// make a GET request to recipe API using recipe ID
// Parse data in nice format

const https = require('https');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

require('dotenv/config');

app.use(bodyParser.json());

const cors = require('cors');
app.use(cors({ origin: true }));
app.options('*', cors());

const port = 3200;

const baseRecipeURL = 'https://api.spoonacular.com/recipes';
const searchURL = '/complexSearch';
const informationURL = '/information';
const apiKey = `apiKey=${process.env.API_KEY}`;

app.get('/api/recipe', async (req, res) => {
    var meal = req.query.meal;
    try {
        var recipeID = await GetRecipesFromAPI(meal);
        var recipe = await GetRecipeFromID(recipeID);
        res.json(recipe);
        console.log(`Returned ${recipe.title} data`);
    } catch (error) {
        res.json({ Error: error });
    }
});

app.listen(port, () => console.log(`Recipe app listening on port ${port}`));

async function GetRecipesFromAPI(meal) {
    const queryURL = baseRecipeURL + searchURL + `/?${apiKey}&query=${meal}`;
    try {
        var recipesJSON = await GetPromise(queryURL);
        return GrabFirstRecipeID(recipesJSON);
    } catch (error) {
        return error;
    }
}

function GrabFirstRecipeID(recipeJSON) {
    var results = recipeJSON.results;
    var firstResult = results[0];
    var id = firstResult.id;
    return id;
}

async function GetRecipeFromID(recipeID) {
    const queryURL =
        baseRecipeURL +
        `/${recipeID}` +
        informationURL +
        `/?${apiKey}&includeNutrition=false`;
    var recipeJSON = await GetPromise(queryURL);
    return ParseRecipeData(recipeJSON);
}

function ParseRecipeData(recipeData) {
    var wantedRecipeData = {};
    wantedRecipeData.title = recipeData.title;

    wantedRecipeData.ingredients = ParseIngredientsData(
        recipeData.extendedIngredients
    );
    wantedRecipeData.instructions = ParseInstructions(
        recipeData.analyzedInstructions[0]
    );
    wantedRecipeData.sourceUrl = recipeData.sourceUrl;
    return wantedRecipeData;
}

function ParseInstructions(instructionData) {
    var instructions = [];
    instructionData.steps.forEach((instruction) => {
        instructions.push(instruction.step);
    });
    return instructions;
}

function ParseIngredientsData(originalIngredientsData) {
    var ingredientsData = [];
    originalIngredientsData.forEach((ingredient) => {
        ingredientsData.push(ingredient.original);
    });
    return ingredientsData;
}

function GetPromise(url) {
    return new Promise((resolve, reject) => {
        https.get(url, (response) => {
            let data = [];

            response.on('data', (chunk) => {
                data.push(chunk);
            });

            response.on('end', () => {
                let responseBody = Buffer.concat(data);
                resolve(JSON.parse(responseBody.toString()));
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}
