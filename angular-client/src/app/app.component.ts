import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { RestService } from './rest.service';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(private restService: RestService) { }

    form = new FormGroup({
        cuisine: new FormControl('', Validators.required),
        dietReq: new FormControl('None', Validators.required)
    });

    pairingData: any = '';
    recipeData: any = '';
    cuisineList: any = [];
    dietReqList: any = [];
    mealsInCuisineList: any;
    selectedMeal: any;
    // ----------------CUISINE SERVICE-----------------

    async getCuisines() {
        this.cuisineList = await this.restService.getCuisines().toPromise();
        console.log(this.cuisineList);
    }
    async getDietReqs() {
        this.dietReqList = await this.restService.getDietReqs().toPromise();
        console.log(this.dietReqList);
    }
    async getMealsInCuisine(cuisine: string, dietReq: string) {
        this.mealsInCuisineList = await this.restService.getMealInCuisine(cuisine, dietReq).toPromise();
        console.log(this.mealsInCuisineList);
    }

    // ----------------PAIRING SERVICE-----------------

    async getPairing(meal: string) {
        if (meal !== '') {
            this.pairingData = await this.restService.getPairing(meal).toPromise();
            console.log('Data: ' + JSON.stringify(this.pairingData));
        }
    }

    // ----------------RECIPE SERVICE-----------------

    async getRecipe(meal: string) {
        if (meal !== '') {
            this.recipeData = await this.restService.getRecipe(meal).toPromise();
            console.log('Data: ' + JSON.stringify(this.recipeData));
        }
    }

    // ----------------FORM-----------------

    get f() {
        return this.form.controls;
    }

    submit() {
        console.log(this.form.value);
        this.getMealsInCuisine(this.f.cuisine.value, this.f.dietReq.value);
        console.log(this.mealsInCuisineList);
    }

    // ----------------INIT-----------------

    ngOnInit() {
        this.getCuisines();
        this.getDietReqs();
    }

}
