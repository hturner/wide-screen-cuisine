const { time } = require('console');
const http = require('http');
const prompt = require('prompt-sync')();

function getPromise(url) {
    return new Promise((resolve, reject) => {
        http.get(url, (response) => {
            let data = [];

            response.on('data', (chunk) => {
                data.push(chunk);
            });

            response.on('end', () => {
                let responseBody = Buffer.concat(data);
                resolve(responseBody.toString());
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}

async function makeSynchronousRequest(url) {
    try {
        let httpPromise = getPromise(url);
        let responseBody = await httpPromise;
        console.log(responseBody);
    } catch (error) {
        console.log('Error: ' + error);
    }
}

const port = 3200;
(async function () {
    var meal = prompt('Enter a meal: ');
    console.time('timer');
    await makeSynchronousRequest(
        `http://localhost:${port}/api/recipe/?meal=${meal}`
    );
    console.timeEnd('timer');
})();
