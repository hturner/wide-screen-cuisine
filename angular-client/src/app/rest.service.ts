import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class RestService {
    constructor(private http: HttpClient) { }

    // ----------------CUISINE SERVICE-----------------

    cuisineURL = 'http://localhost:3100/api';
    pairingsURL = 'http://localhost:3000/api/pairings';
    recipeURL = 'http://localhost:3200/api/recipe/';
    // ----------------CUISINE SERVICE-----------------
    getCuisines() {
        return this.http.get(this.cuisineURL + '/allCuisines');
    }
    getDietReqs() {
        return this.http.get(this.cuisineURL + '/allDietaryRequirements');
    }
    getMealInCuisine(cuisine: string, dietReq: string): Observable<object> {
        return this.http.get(this.cuisineURL + '/cuisine/?cuisine=' + cuisine + '&dietaryRequirement=' + dietReq);
    }

    // ----------------PAIRING SERVICE-----------------

    getPairing(query: string): Observable<object> {
        return this.http.get(this.pairingsURL + '?meal=' + query);
    }

    // ----------------EXTERNAL RECIPE SERVICE-----------------
    getRecipe(query: string): Observable<object> {
        const reqQuery = query.replace(/ /g, '%20');
        return this.http.get(this.recipeURL + '?meal=' + reqQuery);
    }

}
