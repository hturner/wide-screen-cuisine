const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors({ origin: true }));
app.options('*', cors());

const port = 3100;

const meals = require('./meals.json');
const dietaryRequirements = require('./dietaryRequirements.json');

const None = 'None';
const Pescatarian = 'Pescatarian';
const Vegetarian = 'Vegetarian';

app.get('/api/allCuisines', (req, res) => {
    res.send(getAllCuisines());
    console.log('Returned all cuisines');
});

app.get('/api/allDietaryRequirements', (req, res) => {
    res.send(getAllDietaryRequirements());
    console.log('Returned all dietary requirements');
});

app.get('/api/cuisine', (req, res) => {
    var cuisine = req.query.cuisine;
    var dietaryRequirement = req.query.dietaryRequirement;

    var dietaryMeals = meals[cuisine];
    var outputMeals = buildCuisineMeal(dietaryMeals, dietaryRequirement);

    res.send(outputMeals);
    console.log(`Returned ${cuisine}-${dietaryRequirement}`);
});

app.listen(port, () => console.log(`App listening on port ${port}`));

function buildCuisineMeal(cuisine, dietaryRequirements) {
    switch (dietaryRequirements) {
        case None:
            return getAllMeals(cuisine);
        case Pescatarian:
            return collateDietTypes(cuisine[Pescatarian], cuisine[Vegetarian]);
        default:
            return collateDietTypes(cuisine[Vegetarian]);
    }
}

function collateDietTypes() {
    var allMeals = [];
    for (
        var dietTypeIndex = 0;
        dietTypeIndex < arguments.length;
        dietTypeIndex++
    ) {
        var dietMeals = arguments[dietTypeIndex];
        dietMeals.forEach((meal) => {
            allMeals.push(meal);
        });
    }
    return allMeals;
}

function getAllMeals(cuisine) {
    return collateDietTypes(
        cuisine[None],
        cuisine[Pescatarian],
        cuisine[Vegetarian]
    );
}

function getAllCuisines() {
    var cuisines = [];
    for (var key in meals) cuisines.push(key);
    return cuisines;
}

function getAllDietaryRequirements() {
    var dietReqs = [];
    dietaryRequirements['Dietary Requirements'].forEach((dietReq) => {
        dietReqs.push(dietReq);
    });
    return dietReqs;
}
