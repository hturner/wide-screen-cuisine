const http = require('http');
const prompt = require('prompt-sync')();

function getPromise(url) {
    return new Promise((resolve, reject) => {
        http.get(url, (response) => {
            let data = [];

            response.on('data', (chunk) => {
                data.push(chunk);
            });

            response.on('end', () => {
                let responseBody = Buffer.concat(data);
                resolve(responseBody.toString());
            });

            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}

async function makeSynchronousRequest(url) {
    try {
        let httpPromise = getPromise(url);
        let responseBody = await httpPromise;
        console.log(responseBody);
    } catch (error) {
        console.log('Error: ' + error);
    }
}

(async function () {
    await makeSynchronousRequest(`http://localhost:3100/api/allCuisines`);
    var cuisine = prompt('Enter a cuisine: ');
    await makeSynchronousRequest(
        `http://localhost:3100/api/allDietaryRequirements`
    );
    var dietaryRequirement = prompt('Enter a dietary requirement: ');
    await makeSynchronousRequest(
        `http://localhost:3100/api/cuisine/?cuisine=${cuisine}&dietaryRequirement=${dietaryRequirement}`
    );
})();
