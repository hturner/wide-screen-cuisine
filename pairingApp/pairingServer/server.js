const express = require('express');
const app = express();
app.use(express.json());

const cors = require('cors');
app.use(cors({ origin: true }));
app.options('*', cors());

const pairings = require('./pairings.json');

function PairingIncludeMeat(meal, pairing) {
    var pairingIncludesMeat = false;
    pairing.meats.forEach((meat) => {
        const includesMeat = meal.includes(meat);
        if (includesMeat) {
            pairingIncludesMeat = true;
        }
    });
    return pairingIncludesMeat;
}

app.get('/api/pairings/', async (req, res) => {
    const meal = req.query.meal;
    console.log(meal);
    for (let i = 0; i < pairings.pairings.length; i++) {
        const pairing = pairings.pairings[i];
        if (PairingIncludeMeat(meal, pairing)) {
            res.json(pairing);
            break;
        }
        if (i != pairings.pairings.length - 1) continue;
        res.json(pairing);
        break;
    }
});

// Define port to run app on - If no env var then use 3000
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}.`));
